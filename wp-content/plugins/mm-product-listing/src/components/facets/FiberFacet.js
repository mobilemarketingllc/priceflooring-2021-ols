import React from "react";

export default function FiberFacet({ handleFilterClick, productFiber }) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productFiber = sortObject(productFiber);

  return (
    <div class="facet-wrap facet-display">
      <strong>Fiber</strong>
      <div className="facetwp-facet">
        {Object.keys(productFiber).map((fiber, i) => {
          if (fiber && productFiber[fiber] > 0) {
            return (
              <div>
                <span
                  id={`fiber-filter-${i}`}
                  key={i}
                  data-value={`${fiber.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("fiber", e.target.dataset.value)
                  }>
                  {" "}
                  {fiber} {` (${productFiber[fiber]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
