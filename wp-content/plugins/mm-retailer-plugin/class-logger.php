<?php

trait WP_Example_Logger
{

	/**
	 * Really long running process
	 *
	 * @return int
	 */
	public function really_long_running_task()
	{
		return sleep(1);
	}

	/**
	 * Log
	 *
	 * @param string $message
	 */
	public function log($message)
	{
		error_log($message);
	}

	/**
	 * Get lorem
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	protected function get_message($name, $id)
	{

		$msg = $name . '-' . $id . ' Inserted Successfully';

		return $msg;
	}

	/**
	 * Insert Product 
	 *
	 * @param string $message
	 */
	public function insert_product($data, $main_category)
	{

		global $wpdb;
		$table_redirect = $wpdb->prefix . 'redirection_items';
		$table_group = $wpdb->prefix . 'redirection_groups';
		$table_posts = $wpdb->prefix . 'posts';
		$table_meta = $wpdb->prefix . 'postmeta';
		$main_cat = '';
		$find_sku = $data['sku'];

		//write_log($data['in_stock'].'------'.get_option('instocksyncseprate'));

		$sql_sku = "SELECT $table_posts.ID
						FROM $table_posts
						WHERE $table_posts.post_type = '" . $main_category . "' AND $table_posts.post_content = '" . $find_sku . "'";

		$duplicates = $wpdb->get_results($sql_sku, ARRAY_A);

		if ($data['status'] == 'active' && trim(@$data['swatch']) != "") {

				$last_seg = '';

			    $curr_url = $data['url_key'];

				$parsed_url = wp_parse_url($curr_url);

				if (isset($parsed_url['path'])) {

					$path = trim($parsed_url['path'], '/');

					$segment = explode('/', $path);

					$last_seg = end($segment);
				}


			// do something if the sku exists in another post
			if (count($duplicates) > 0) {

				$id = $duplicates[0]['ID'];									

				// update the post into the database.
				$up_post = array(

					'ID'           => $id,	
					'post_title'    => $data['name']. ' '. $data['sku'],
					'post_name'    => $last_seg,									
					'post_status'  => 'publish'							
				);	

				$post_id = wp_update_post( $up_post );	

				$wpdb->query('DELETE FROM ' . $table_redirect . ' WHERE title = "' . $data['sku'] . '"');

				$wpdb->query('DELETE FROM ' . $table_redirect . ' WHERE url LIKE "%' . $data['sku'] . '%"');
				
			} else {

				
				if (@$data['in_stock'] == '1' && get_option('instocksyncseprate') == '1') {

					//write_log('instockproduct');

					if ($main_category == 'hardwood_catalog') {

						@$main_cat = 'instock_hardwood';
					} elseif ($main_category == 'carpeting') {

						@$main_cat = 'instock_carpet';
					} elseif ($main_category == 'laminate_catalog') {

						@$main_cat = 'instock_laminate';
					} elseif ($main_category == 'luxury_vinyl_tile') {

						@$main_cat = 'instock_lvt';
					} elseif ($main_category == 'tile_catalog') {

						@$main_cat = 'instock_tile';
					} elseif ($main_category == 'sheet') {

						@$main_cat = 'instock_lvt';
					} elseif ($main_category == 'sheet_vinyl') {

						@$main_cat = 'instock_sheet_vinyl';
					} elseif ($main_category == 'area_rugs') {

						@$main_cat = 'instock_area_rugs';
					}
				}

				if ($main_cat != '') {

					$main_category = $main_cat;
				}

				write_log($main_category);

				$my_post = array(

					'post_title'    => $data['name'] . ' ' . $data['sku'],
					'post_content'  => $data['sku'],
					'post_type'  => $main_category,
					'post_excerpt' => $data['manufacturer'],
					'post_status'   => 'publish',
					'post_name'   => $last_seg,
					'post_author'   => 1

				);

				$wpdb->query('DELETE FROM ' . $table_redirect . ' WHERE title = "' . $data['sku'] . '"');

				$wpdb->query('DELETE FROM ' . $table_redirect . ' WHERE url LIKE "%' . $data['sku'] . '%"');


				// Insert the post into the database.
				$post_id = wp_insert_post($my_post);
			}
		} elseif ($data['status'] == 'inactive' || trim(@$data['swatch']) != "") {


			if (count($duplicates) > 0) {

				$source_url = wp_make_link_relative(get_the_guid($duplicates[0]['ID']));
				$match_url = rtrim($source_url, '/');
				$post_type =  $main_category;

				$urlmapping = array(
					"/flooring/carpet/products/" => "carpeting",
					"/flooring/hardwood/products/" => "hardwood_catalog",
					"/flooring/laminate/products/" => "laminate_catalog",
					"/flooring/" => "luxury_vinyl_tile",
					"/flooring/tile/products/" => "tile_catalog",
					"/flooring/paint/products/" => "paint_catalog",
					"/flooring/area-rugs/products/" => "area_rugs",
					"/flooring/waterproof/" => "solid_wpc_waterproof"
				);

				$destination_url = array_search($post_type, $urlmapping);
				write_log($destination_url);

				$data = array(
					"url" => $source_url,
					"match_url" => $match_url,
					"match_data" => "",
					"action_code" => "301",
					"action_type" => "url",
					"action_data" => $destination_url,
					"match_type" => "url",
					"title" => $find_sku,
					"regex" => "true",
					"group_id" => '1',
					"position" => "1",
					"last_access" => current_time('mysql'),
					"status" => "enabled"
				);



				$format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

				$wpdb->insert($table_redirect, $data, $format);

				write_log('Product need to delete');

				wp_delete_post($duplicates[0]['ID']);
			}
		}
	}
}
